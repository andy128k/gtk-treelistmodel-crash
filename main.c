#include <gtk/gtk.h>

#include <stdlib.h>

typedef struct _DemoNode DemoNode;

struct _DemoNode {
  GObject parent_instance;
  gchar *value;
  GListStore *children;
};

G_DECLARE_FINAL_TYPE (DemoNode, demo_node, DEMO, NODE, GObject);

G_DEFINE_TYPE (DemoNode, demo_node, G_TYPE_OBJECT);

enum {
  PROP_0,
  PROP_VALUE,

  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL, };

static void
demo_node_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  DemoNode *self = DEMO_NODE(object);

  switch (property_id)
    {
    case PROP_VALUE:
      g_value_set_string(value, self->value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
      break;
    }
}

static void demo_node_finalize (GObject *object) {
  DemoNode *self = DEMO_NODE (object);
  g_free (self->value);
  G_OBJECT_CLASS (demo_node_parent_class)->finalize (object);
}

static void demo_node_class_init(DemoNodeClass *klass) {
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->finalize = demo_node_finalize;
  gobject_class->get_property = demo_node_get_property;

  properties[PROP_VALUE] = g_param_spec_string("value", NULL, NULL, NULL, G_PARAM_READABLE);
  g_object_class_install_properties (gobject_class, 2, properties);
}

static void demo_node_init (DemoNode *self) {
}

static DemoNode * demo_node_new(gchar * value, GListStore *children) {
  DemoNode *result = g_object_new(demo_node_get_type(), NULL);
  result->value = g_strdup (value);
  result->children = children;
  return result;
}

static GListStore* create_model() {
  DemoNode* aa = demo_node_new("aa", NULL);

  GListStore* a_children = g_list_store_new(demo_node_get_type());
  g_list_store_append(a_children, aa);

  DemoNode* a = demo_node_new("a", a_children);
  DemoNode* b = demo_node_new("b", NULL);
  DemoNode* c = demo_node_new("c", NULL);

  GListStore* root = g_list_store_new(demo_node_get_type());
  g_list_store_append(root, a);
  g_list_store_append(root, b);
  g_list_store_append(root, c);
  return root;
}

static GListModel * model_children(gpointer item, gpointer unused) {
  GListStore* children = DEMO_NODE(item)->children;
  if (children) {
    return G_LIST_MODEL(g_object_ref(children));
  } else {
    return NULL;
  }
}

static GListStore *model = NULL;
static GtkWidget *window = NULL;

static void add_clicked(void) {
  DemoNode * a = DEMO_NODE(g_list_model_get_item(G_LIST_MODEL(model), 0));
  printf("add to %s\n", a->value);

  DemoNode* ab = demo_node_new("ab", NULL);
  g_list_store_append(a->children, ab);
}

void do_activate(GApplication *app) {
  if (window == NULL) {
    GtkWidget *listview;
    GtkTreeListModel *treemodel;
    GtkSingleSelection *selection;
    GtkButton* add_button;
    GtkBuilder *builder;

    g_type_ensure(demo_node_get_type());

    builder = gtk_builder_new ();

    gtk_builder_add_from_resource (builder, "/com/example/gtk-treelistmodel-crash/crash.ui", NULL);

    window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
    g_object_add_weak_pointer (G_OBJECT (window), (gpointer *) &window);
    gtk_application_add_window (GTK_APPLICATION(app), GTK_WINDOW(window));

    listview = GTK_WIDGET (gtk_builder_get_object (builder, "listview"));

    model = create_model();
    treemodel = gtk_tree_list_model_new (G_LIST_MODEL(model),
                                          FALSE,
                                          FALSE,
                                          model_children,
                                          NULL,
                                          NULL);
    selection = gtk_single_selection_new (G_LIST_MODEL (treemodel));
    gtk_list_view_set_model (GTK_LIST_VIEW (listview), GTK_SELECTION_MODEL (selection));
    g_object_unref (selection);

    add_button = GTK_BUTTON (gtk_builder_get_object (builder, "add_button"));
    g_signal_connect (G_OBJECT(add_button), "clicked", G_CALLBACK(add_clicked), NULL);

    g_object_unref (builder);
  }

  gtk_widget_show (window);
}

int main() {
  GtkApplication *app;

  app = gtk_application_new("com.exanple.gtk-treelistmodel-crash", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(do_activate), NULL);
  g_application_run(G_APPLICATION(app), 0, NULL);
}
